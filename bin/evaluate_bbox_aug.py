#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Predict bbox')
parser.add_argument('-i', '--images', nargs='+',
                    help='The path to the image to predict')
parser.add_argument('-l', '--masks', nargs='+',
                    help='The path to the mask images of the images')
parser.add_argument('-m', '--model', required=True,
                    help='The path to the model')
parser.add_argument('-o', '--output-dir', required=False, default='.')
args = parser.parse_args()

import os
import numpy as np
import nibabel as nib
import pandas as pd
from keras.models import load_model
from keras_contrib.layers import InstanceNormalization
from image_processing_3d import scale3d, translate3d_int, calc_bbox3d

from keras_unet_cerebellum import calc_smooth_l1_loss


def convert_mask_to_bbox(mask):
    bbox_slices = calc_bbox3d(mask)
    bbox = list()
    for bs in bbox_slices:
        bbox.append(bs.start)
        bbox.append(bs.stop)
    return bbox

custom_objects = {'calc_smooth_l1_loss': calc_smooth_l1_loss,
                  'InstanceNormalization': InstanceNormalization}
model = load_model(args.model, custom_objects=custom_objects)

if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)

for image_filename, mask_filename in zip(args.images, args.masks):

    print(image_filename, mask_filename)

    image_obj = nib.load(image_filename)
    image = image_obj.get_data()

    mask_obj = nib.load(mask_filename)
    mask = (mask_obj.get_data() > 0).astype(float)

    truth_bbox = convert_mask_to_bbox(mask)
    pred_bbox = model.predict(image[None, None, ...])[0, ...]
    error = np.abs(truth_bbox - pred_bbox)

    col_names = ['x1', 'x2', 'y1', 'y2', 'z1', 'z2']
    ind_names = ['pred', 'error']

    df = pd.DataFrame.from_records([pred_bbox, error], index=ind_names,
                                   columns=col_names)

    basename = os.path.basename(image_filename)
    basename = basename.replace('.nii.gz', '_bbox.csv')
    filename = os.path.join(args.output_dir, basename)
    df.to_csv(filename)
