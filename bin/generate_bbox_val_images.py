#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Predict bbox')
parser.add_argument('-i', '--image-dir',
                    help='The path to the images and corresponding. Image is '
                         '${id}_image.nii.gz and mask is ${id}_mask.nii.gz')
parser.add_argument('-a', '--augmentation', nargs='+',
                    default=['none', 'translation', 'scaling'],
                    choices=['none', 'translation', 'scaling'],
                    help='The augmentation used')
parser.add_argument('-o', '--output-dir', required=False, default='aug_data')
args = parser.parse_args()

import os
from glob import glob
import numpy as np
import nibabel as nib
from image_processing_3d import scale3d, translate3d_int


if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)

image_paths = sorted(glob(os.path.join(args.image_dir, '*_image.*')))
mask_paths = sorted(glob(os.path.join(args.image_dir, '*_mask.*')))

num_images = len(image_paths)
num_perturbation = np.ceil(num_images ** (1/3) / 2).astype(int)
translations = np.linspace(5, 15, num_perturbation).astype(int)
translations = np.hstack([-translations, translations])
scales = np.linspace(1.1, 1.3, num_perturbation)
scales = np.hstack([1/scales, scales])

x_trans, y_trans, z_trans = np.meshgrid(*[translations]*3)
x_trans = x_trans.flatten()[..., None]
y_trans = y_trans.flatten()[..., None]
z_trans = z_trans.flatten()[..., None]
translations = np.hstack([x_trans, y_trans, z_trans])

x_scale, y_scale, z_scale = np.meshgrid(*[scales]*3)
x_scale = x_scale.flatten()[..., None]
y_scale = y_scale.flatten()[..., None]
z_scale = z_scale.flatten()[..., None]
scales = np.hstack([x_scale, y_scale, z_scale])

for i in range(len(image_paths)):
    image_path = image_paths[i]
    mask_path = mask_paths[i]
    trans = translations[i]
    scale = scales[i]

    image_obj = nib.load(image_path)
    image = image_obj.get_data()
    image_base = os.path.basename(image_path)

    mask_obj = nib.load(mask_path)
    mask = (mask_obj.get_data() > 0).astype(float)
    mask_base = os.path.basename(mask_path)

    if 'none' in args.augmentation:
        out_im_obj = nib.Nifti1Image(image, image_obj.affine, image_obj.header)
        out_mask_obj = nib.Nifti1Image(mask, mask_obj.affine, mask_obj.header)
        out_im_path = os.path.join(args.output_dir,
                                   image_base.replace('.nii', '_none.nii'))
        out_mask_path = os.path.join(args.output_dir,
                                     mask_base.replace('.nii', '_none.nii'))
        out_im_obj.to_filename(out_im_path)
        out_mask_obj.to_filename(out_mask_path)

    if 'translation' in args.augmentation:
        x_trans, y_trans, z_trans = trans
        im_tmp = translate3d_int(image, x_trans, y_trans, z_trans)
        mask_tmp = translate3d_int(mask, x_trans, y_trans, z_trans)
        sub = '_trans_%d_%d_%d.nii' % (x_trans, y_trans, z_trans)
        out_im_basename = image_base.replace('.nii', sub)
        out_im_path = os.path.join(args.output_dir, out_im_basename)
        out_mask_base = mask_base.replace('.nii', sub)
        out_mask_path = os.path.join(args.output_dir, out_mask_base)
        out_im_obj = nib.Nifti1Image(im_tmp, image_obj.affine,
                                     image_obj.header)
        out_mask_obj = nib.Nifti1Image(mask_tmp, mask_obj.affine,
                                       mask_obj.header)
        out_im_obj.to_filename(out_im_path)
        out_mask_obj.to_filename(out_mask_path)

    if 'scaling' in args.augmentation:
        x_scale, y_scale, z_scale = scale
        im_tmp = scale3d(image, x_scale, y_scale, z_scale)
        mask_tmp = scale3d(mask, x_scale, y_scale, z_scale, order=0)
        sub = '_scale_%.2f_%.2f_%.2f.nii' % (x_scale, y_scale, z_scale)
        out_im_base = image_base.replace('.nii', sub)
        out_im_path = os.path.join(args.output_dir, out_im_base)
        out_mask_base = mask_base.replace('.nii', sub)
        out_mask_path = os.path.join(args.output_dir, out_mask_base)
        out_im_obj = nib.Nifti1Image(im_tmp, image_obj.affine,
                                     image_obj.header)
        out_mask_obj = nib.Nifti1Image(mask_tmp, mask_obj.affine,
                                       mask_obj.header)
        out_im_obj.to_filename(out_im_path)
        out_mask_obj.to_filename(out_mask_path)
