# -*- coding: utf-8 -*-

import numpy as np


class DataGeneratorFactory:
    """Create data generator

    Attributes:
        shuffle (bool): Shuffle the order of the data
        batch_size (int): The number of samples each time the generator yields

    """
    def __init__(self, shuffle=True, batch_size=1):
        self.shuffle = shuffle
        self.batch_size = batch_size

    def create(self, dataset):
        """Create data generator

        Yield data with random order

        Args:
            dataset (object): Implement __getitem__ and __len__

        Returns:
            data_generator (generator function): Check python `yield`

        """
        while True:
            if self.shuffle:
                indices = np.random.choice(len(dataset), len(dataset),
                                           replace=False)
            else:
                indices = np.arange(len(dataset))
            for ind in indices:
                images = list()
                labels = list()
                for i in range(self.batch_size):
                    image, label = dataset[ind]
                    images.append(image[None, ...])
                    labels.append(label[None, ...])
                image = np.vstack(images)
                label = np.vstack(labels)
                yield image, label
