#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '..')

from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import os
from network_utils import TrainingDataFactory as DF
from network_utils import Dataset3dFactory as DSF

from keras_unet_cerebellum import Configuration
from keras_unet_cerebellum.networks.bounding_box import BboxFactoryDecorator


image_paths = sorted(glob('../bin/data/*_image.*'))[:1]
mask_paths = sorted(glob('../bin/data/*_mask.*'))[:1]

# augmentation = ['none', 'flipping', 'rotation', 'deformation']
augmentation = ['none', 'rotation']
config = Configuration()
data_factory = DF(dim=config.channel_axis, label_pairs=[],
                  max_angle=10, get_data_on_the_fly=False, types=augmentation)
data_factory = BboxFactoryDecorator(data_factory)
t_dataset, v_dataset = DSF.create(data_factory, [], image_paths, mask_paths)

print('-' * 80)
print('TRAINING')
for d in t_dataset.data:
    print(*[dd.filepath for dd in d])
print('Number:', len(t_dataset))

print('-' * 80)
print('VALIDATION')
for d in v_dataset.data:
    print(*[dd.filepath for dd in d])
print('Number:', len(v_dataset))
print('-' * 80)

for image, bbox in t_dataset:

    image = image[0, ...]
    print(bbox)
    mask = np.zeros(image.shape)
    mask[bbox[0]:bbox[1], bbox[2]:bbox[3], bbox[4]:bbox[5]] = 1

    plt.figure()

    plt.subplot(1, 3, 1)
    sliceid = image.shape[0] // 2
    plt.imshow(image[sliceid, :, :], cmap='gray', alpha=0.7)
    plt.imshow(mask[sliceid, :, :], alpha=0.3)
    plt.subplot(1, 3, 2)
    sliceid = image.shape[1] // 3 * 2
    plt.imshow(image[:, sliceid, :], cmap='gray', alpha=0.7)
    plt.imshow(mask[:, sliceid, :], alpha=0.3)
    plt.subplot(1, 3, 3)
    sliceid = image.shape[2] // 3
    plt.imshow(image[:, :, sliceid], cmap='gray', alpha=0.7)
    plt.imshow(mask[:, :, sliceid], alpha=0.3)

plt.show()
