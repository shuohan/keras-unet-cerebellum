#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '..')

import numpy as np
from copy import deepcopy
from keras import backend as K
from scipy.ndimage.measurements import find_objects

from keras_unet_cerebellum.dice import calc_aver_dice, calc_aver_dice_loss

image = np.zeros((3, 10, 10, 10))

def put_square(image, size, channel_id):
    dims = list()
    for s in image.shape[1:]:
        start = (s - size) // 2
        stop = start + size
        dims.append(slice(start, stop, None))
    image[(channel_id, *dims)] = 1

put_square(image, 2, 0)
put_square(image, 4, 1)
put_square(image, 8, 2)

def shift(image, shifts, axis, channel_id):
    shifted = np.zeros(image.shape)
    orig_location = find_objects(image[channel_id, ...], 1)[0]
    orig_slice = orig_location[axis]
    new_location = list(deepcopy(orig_location))
    new_slice = slice(orig_slice.start + shifts, orig_slice.stop + shifts, None)
    new_location[axis] = new_slice
    shifted[(channel_id, *new_location)] = image[(channel_id, *orig_location)]
    other_channels = list(set(range(image.shape[0])) - {channel_id})
    shifted[other_channels, ...] = image[other_channels, ...]
    return shifted

shifted = shift(image, 1, 1, 0)
shifted = shift(shifted, 2, 2, 1)
shifted = shift(shifted, -1, 0, 2)

def print_image(image):
    for channel in image:
        for i in range(channel.shape[-1]):
            print(channel[:, :, i])
        print('------')

d = calc_aver_dice(image, shifted)
print(K.eval(d))

d = calc_aver_dice_loss(image, shifted, eps=0)
print(K.eval(d))
assert K.eval(d) == 1 - 1/3 * (1/2 + 1/2 + 7/8)
